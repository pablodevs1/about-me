<div align="center">
<h1> Hey there visitor! <img src="https://raw.githubusercontent.com/MartinHeinz/MartinHeinz/master/wave.gif" width="30px"></h1>
</div>

## Is there something that makes me different? 🤔

I love solving challenges, as a kid with my rubik cubes and now working as a researcher at the department of applied physics. Thanks to my scientific background I have a particular point of view when I'm coding. I'm able to find similarities between both branches which helps me understanding faster and more efficiently. however, programming isn't my only passion, I've always found a balance between work and my hobbies: excercising and music, playing the guitar in particular.

## What is Git for me? ⛓️

We are told that it is a version control system, and no more. However, there were other solutions before Git, for example CVS. That's why I wouldn't be able to explain it better than its creator, Linus Torvalds. As he said, as a project (in this case Linux) grows from 100 to 10,000 people, a time during which most of them contribute with small changes, something radical is then needed to maintain the project. Here comes Git, the solution to control changes in these big projects.

## What is Docker for me? 🐋

When hearing about containers, I often hear the word "Docker". After attending the course "Introduction to Containers w/ Docker, Kubernetes & OpenShift" of IBM, I understand Docker as an open-source platform for building and running applications as containers.

A container is a standard executable unit of software in which application code is packaged, along with its libraries and dependencies so that it can be run anywhere, whether on a desktop, on-premises, or in the cloud.

## What is Testing for me? ✅

Testing is an important part of the creation of new features in software development. It is used not only to find potential bugs in the code, but also to check compatibility, security, and performance. Without testing, how could we know for example if the API supports a high number of requests per second?

## What Javascript features do I know right now? 🧮

As far as I know, Javascript is:
- a scripting programming language,
- object-oriented,
- and “weakly typed” or “untyped”

Some JS features:
- **Call Stack**: allows the JS engines to know in which function they are in and what functions they previously executed to get there.
- **Scope**: JavaScript variables can belong to the local or global scope, it refers to the current execution context. Where values and expressions are "visible" or can be referenced.
- **Closures**: functions that have access to the parent function scope, even after the parent function has closed.
- **Babel**: open-source JS transcompiler used to convert ES6+ code into a backwards compatible version of JavaScript that can be run by older JavaScript engines.
- **ES6**: major revision to JavaScript. ES6 Features (some that I consider useful):
    - Promises
    - Let and Const
    - Arrow functions
    - Classes
    - Modules
    - Symbol
    - Map() and Set()
    - Template Literals
    - Destructuring Assignment

## What are Web Components for me? 🔄

In my opinion, web components are reusable custom logic, piece of code, that has its functionality enclosed from the rest of the code in the app.

## Why do I want to start working at KairosDS? 🧑‍💻

Kairós has caught my eye since the very beginning: after reading the whole summary of the V Generation K, I did not hesitate to sign up for this offer. Apart from the "Great Place To Work" awards, Kairós offers many advantages over other companies such as the opportunity for me to keep learning with courses, finding out throughout experience in which team I would fit and contribute best, having a medical insurance, and much more...
